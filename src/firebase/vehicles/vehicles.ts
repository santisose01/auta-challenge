import { deleteDoc, doc, getDoc, setDoc, updateDoc } from "firebase/firestore";
import db from "../config.ts";

export const createVehicle = async (vehicle) => {
  try{

    const { name, initial_price, image_url, characteristics } = vehicle;

    const vehicleSearch = await getDoc(doc(db, 'vehicle', name));
    if(vehicleSearch.exists()){ return 'name_registered' };
    await setDoc(doc(db, 'vehicle', name), {
      name,
      initial_price,
      image_url,
      characteristics,
      status: 'enabled'
    });

    window.location.reload();

  } catch (error) {
  console.log(error);
  }
}

export const changeFavoriteVehicle = async (userEmail, vehicleName, isFavorite) => {
  try{

    getDoc(doc(db, 'user', userEmail)).then(async (user) => {
      if(isFavorite){
        console.log(user, user.data())
        const newFavorite = user.data()?.favorite_vehicles.filter(vehicle => vehicle != vehicleName);
        await updateDoc(doc(db, 'user', userEmail), {
          favorite_vehicles: newFavorite
        });
      }else{
        let newFavorite = user.data()?.favorite_vehicles;
        newFavorite.unshift(vehicleName);
        await updateDoc(doc(db, 'user', userEmail), {
          favorite_vehicles: newFavorite
        });
      }
    });    

  } catch (error) {
  console.log(error);
  }
}

export const deleteVehicle = async (vehicleName) => {
  try{
        
    await deleteDoc(doc(db, 'vehicle', vehicleName));
    window.location.reload();

  } catch (error) {
  console.log(error);
  }
}

export const enableVehicle = async (vehicleName) => {
  try{
        
    await updateDoc(doc(db, 'vehicle', vehicleName), {
      status: 'enabled'
    });   
    window.location.reload(); 

  } catch (error) {
  console.log(error);
  }
}

export const disableVehicle = async (vehicleName) => {
  try{
        
    await updateDoc(doc(db, 'vehicle', vehicleName), {
      status: 'disabled'
    });    
    window.location.reload();


  } catch (error) {
  console.log(error);
  }
}

export const deleteAllFavoriteVehicles = async (userEmail) => {
  try{
        
    await updateDoc(doc(db, 'user', userEmail), {
      favorite_vehicles: []
    });    

  } catch (error) {
  console.log(error);
  }
}

export const updateVehicle = async (vehicleName, data) => {
  try{
      
    await updateDoc(doc(db, 'vehicle', vehicleName), data);    
    window.location.reload();

  } catch (error) {
  console.log(error);
  }
}
