import bcrypt from 'bcryptjs';
import { doc, setDoc, getDoc } from "firebase/firestore";
import db from '../config.ts';
import { generateAccessToken } from '../jwtoken/jwtoken.ts';

export const LoginMiddleware = async (data) => {
    try{

      const { email, password } = data;

      //Validate Email
      const userSearch = await getDoc(doc(db, 'user', email));
      if(!userSearch.exists()){ return 'no_exists' };

      //Validate Password
      const comparePass = bcrypt.compareSync(password, userSearch.data().password);
      if(!comparePass) {return 'pass_error'};

      //Create Session Token
      const accessToken = await generateAccessToken({ email });
      if(accessToken) localStorage.setItem('sessionToken', accessToken);

      window.location.assign('/');
      
    } catch (error) {
		console.log(error);
    }
}

export const RegisterMiddleware = async (data) => {
    try{

      const { name, lastname, email, password, role } = data;

      let passwordHash = await bcrypt.hash(password, 8);
      const userSearch = await getDoc(doc(db, 'user', email));
      if(userSearch.exists()){ return 'email_registered' };
      await setDoc(doc(db, 'user', email), {
        name,
        lastname,
        email,
        password: passwordHash,
        role,
        favorite_vehicles: [],
        chats: []
      });

      const accessToken = await generateAccessToken({ email });
      if(accessToken) localStorage.setItem('sessionToken', accessToken);

      window.location.assign('/');

    } catch (error) {
		console.log(error);
    }
}


export const LogoutMiddleware = async () => {
    try{

      localStorage.removeItem('sessionToken');
      window.location.replace('/login');

    } catch (error) {
		console.log(error);
    }
}
