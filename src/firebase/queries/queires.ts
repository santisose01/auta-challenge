import { doc, getDoc, setDoc, updateDoc } from "firebase/firestore";
import db from "../config.ts";
import { v4 } from 'uuid';

export const redirectNewChat = async (vehicleName) => {
  try{

      window.location.replace(`/chat?vehicle=${vehicleName}`);

  } catch (error) {
  console.log(error);
  }
}

export const getChat = async (key:any) => {
  try{

    return await getDoc(doc(db, 'query', key)).then((user:any) => {
      return user.data()
    });  

  } catch (error) {
  console.log(error);
  }
}

export const createQuery = async (userEmail, vehicleName, firstChat) => {
  try{

    const currentDate = new Date();
    const key = v4();

    await setDoc(doc(db, 'query', key), {
        cl_email: userEmail,
        vehicle_name: vehicleName,
        status: 'pending',
        chat: [
          {
            role: 'cl',
            date: currentDate.toLocaleString(),
            message: firstChat
          }
        ]
    });

    await getDoc(doc(db, 'user', userEmail)).then(async (user) => {
        const newChats = user.data()?.chats;
        newChats.unshift(key)
        await updateDoc(doc(db, 'user', userEmail), {
          chats: newChats
        });
    });
    
    window.location.replace(`/chat?${key}`)

  } catch (error) {
  console.log(error);
  }
}

export const assignQuery = async (key, userEmail) => {
  try{

    await updateDoc(doc(db, 'query', key), {
      status: 'active',
      as_email: userEmail
    });

    await getDoc(doc(db, 'user', userEmail)).then(async (user) => {
        const newChats = user.data()?.chats;
        newChats.unshift(key)
        await updateDoc(doc(db, 'user', userEmail), {
          chats: newChats
        });
    });
    
    window.location.replace(`/chat?${key}`)

  } catch (error) {
  console.log(error);
  }
}

export const sendMessage = async (key, message, userRole) => {
  try{

    const currentDate = new Date()

    return await getDoc(doc(db, 'query', key)).then(async (query) => {
      const newChat = query.data()?.chat;
      newChat.push({
        role: userRole,
        date: currentDate.toLocaleString(),
        message
      })
      await updateDoc(doc(db, 'query', key), {
        chat: newChat
      });
      return newChat
    });

  } catch (error) {
  console.log(error);
  }
}

export const getQuery = async (userEmail, vehicleName, isFavorite) => {
  try{

    getDoc(doc(db, 'user', userEmail)).then(async (user) => {
      if(isFavorite){
        console.log(user, user.data())
        const newFavorite = user.data()?.favorite_vehicles.filter(vehicle => vehicle != vehicleName);
        await updateDoc(doc(db, 'user', userEmail), {
          favorite_vehicles: newFavorite
        });
      }else{
        let newFavorite = user.data()?.favorite_vehicles;
        newFavorite.unshift(vehicleName);
        await updateDoc(doc(db, 'user', userEmail), {
          favorite_vehicles: newFavorite
        });
      }
    });    

  } catch (error) {
  console.log(error);
  }
}
