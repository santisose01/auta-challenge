import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';
import { getDownloadURL, getStorage, ref, uploadBytes } from 'firebase/storage';
import { v4 } from 'uuid';
import 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyCQkoWtvZK_C2CGypZbCmYqqVhKfQmqX_A",
  authDomain: "autachallenge2024.firebaseapp.com",
  projectId: "autachallenge2024",
  storageBucket: "autachallenge2024.appspot.com",
  messagingSenderId: "621405354277",
  appId: "1:621405354277:web:b380a93ea64de09c2382cc",
  measurementId: "G-RF1JKEG9D1"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export default db;
export const storage = getStorage(app);

export async function uploadFile(file) {
  const storageRef = ref(storage, `vehicles/${v4()}`);
  await uploadBytes(storageRef, file);
  const url = await getDownloadURL(storageRef);
  return url
}