import jwt from 'jsonwebtoken';

export const generateAccessToken = async (user) => {
    try{

        return jwt.sign(user, process.env.REACT_APP_JWT_SECRET || '', {expiresIn: '2h'});
  
    } catch (error) {
    console.log(error);
    }
}

export const validateToken = async () => {
    try{

        const sessionToken = localStorage.getItem('sessionToken');
        if(sessionToken){
            return jwt.verify(sessionToken, process.env.REACT_APP_JWT_SECRET || '', (err, user) => {
                if(err){
                    alert('Token vencido');
                    localStorage.removeItem('sessionToken');
                    window.location.assign('/login');
                }else{
                    return user
                }
            })
        }else{
            return null
        }
      
    } catch (error) {
    console.log(error);
    }
}