import React from 'react';
import './App.scss';
import {
  createBrowserRouter,
  RouterProvider,
} from "react-router-dom";
import './_global.scss';
import Catalog from './screens/catalog/catalog.tsx';
import { AppProvider } from './contexts/AppProvider.tsx';
import Login from './screens/login/login.tsx';
import Register from './screens/register/register.tsx';
import UpdateVehicles from './screens/update-vehicles/update-vehicles.tsx';
import Favorites from './screens/favorites/favorites.tsx';
import { Queries } from './screens/queries/queries.tsx';
import { Chat } from './screens/chat/chat.tsx';
import Vehicle from './screens/vehicle/vehicle.tsx';

function App() {

  const router = createBrowserRouter([
    {
      path: "/",
      element: (<Catalog/>),
    },
    {
      path: "/catalog",
      element: (<Catalog/>),
    },
    {
      path: "/login",
      element: (<Login/>),
    },
    {
      path: "/register",
      element: (<Register/>),
    },
    {
      path: "/update-vehicles",
      element: (<UpdateVehicles/>),
    },
    {
      path: "/favorites",
      element: (<Favorites/>),
    },
    {
      path: "/queries",
      element: (<Queries/>),
    },
    {
      path: "/pending-queries",
      element: (<Queries/>),
    },
    {
      path: "/chat",
      element: (<Chat/>),
    },
    {
      path: "/vehicle",
      element: (<Vehicle/>),
    },
  ]);

  return (
    <div className='App'>
      <AppProvider>
        <RouterProvider router={router}/>
      </AppProvider>
    </div>
  );
}

export default App;
