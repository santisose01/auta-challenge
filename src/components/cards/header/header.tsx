import React, { useContext, useState } from 'react';
import './header.scss';
import { AppContext } from '../../../contexts/AppProvider.tsx';
import { LogoutMiddleware } from '../../../firebase/auth/auth.ts';

export default function Header() {
  
  const [profileOpen, setProfileOpen] = useState(false);
  const { actualUser }:any = useContext(AppContext);

  const origin = window.location.origin;
  const headerMenu = [
    { text: 'Catálogo', link: `${origin}/catalog` },
    { text: actualUser?.name? actualUser.name: 'Iniciar Sesion', link: actualUser?.name? '': `${origin}/login` }
  ];

  return (
    <div className='header-container'>
      <div className='auta-container'>
        <label className='top-text-extra-bold'>AUTA Challenge</label>
        <label className='text-regular'>Santiago Sosa</label>
      </div>
      <div className='menu'>
        {
          headerMenu.map((e) => {
            if(e.link){
              return(
                <div key={e.text} className='text-menu' onClick={() => window.location.assign(e.link)}>
                  <label className='text-medium'>{e.text}</label>
                </div>
              )
            }else{
              return(
                <div key={e.text} className='text-menu' onMouseEnter={() => setProfileOpen(true)} onMouseLeave={() => setProfileOpen(false)}>
                  <label className='text-medium'>Tu perfil</label>
                  {actualUser?.name && profileOpen && (
                    actualUser.role === 'as'?
                    <div className='card text-right profile-menu-as'>
                      <label className='text-regular'>Hola! {actualUser.name}</label>
                      <label className='text-regular' onClick={() => window.location.replace('/favorites')}>Tus Favoritos</label>
                      <label className='text-regular' onClick={() => window.location.replace('/queries')}>Tus Consultas</label>
                      <label className='text-regular' onClick={() => window.location.replace('/pending-queries')}>Consultas Pendientes</label>
                      <label className='text-regular' onClick={() => window.location.replace('/update-vehicles')}>Modificar Vehículos</label>
                      <button onClick={LogoutMiddleware} className='btn btn-light'>Cerrar sesión</button>
                    </div>
                    :
                    <div className='card text-right profile-menu'>
                      <label className='text-regular'>Hola! {actualUser.name}</label>
                      <label className='text-regular' onClick={() => window.location.replace('/favorites')}>Tus Favoritos</label>
                      <label className='text-regular' onClick={() => window.location.replace('/queries')}>Tus Consultas</label>
                      <button onClick={LogoutMiddleware} className='btn btn-light'>Cerrar sesión</button>
                    </div>
                    )
                  }
                </div>
              )
            }
          })
        }
      </div>
    </div>
  );
}
