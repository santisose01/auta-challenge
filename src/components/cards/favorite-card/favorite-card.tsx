import React, { useContext, useState } from 'react'
import './favorite-card.scss';
import { AppContext } from '../../../contexts/AppProvider.tsx';
import { changeFavoriteVehicle } from '../../../firebase/vehicles/vehicles.ts';

export const FavoriteCard = ({ vehicle, userEmail }) => {

  const { name, initial_price, characteristics, image_url } = vehicle;
  
  //Actions States
  const [mouseChat, setMouseChat] = useState(false);
  const [mouseFavorite, setMouseFavorite] = useState(false);
  const [mousePlus, setMousePlus] = useState(false);

  const favoriteClick = async () => {
    await changeFavoriteVehicle(userEmail, name, true);
    setTimeout(() => {
      window.location.reload();
    }, 2000);
  }

  return (
    <div className='favorite-card-container'>
        <div className='left-panel'>
            <div className='image'>
              <img src={image_url} alt={name}/>
            </div>
            <div className='name'>
              <label className='text-regular'>{name}</label>
            </div>
        </div>
        <div className='right-panel'>
            <div className='data'>
              <label className='text-regular initial-cost'>Consultá a partir de</label>
              <div className='price'>
                <i className="bi-currency-dollar subtitle-regular"></i>
                <label className='subtitle-regular'>{initial_price}</label>
              </div>
              <div className='characteristics'>
                <label className='top-text-regular'>Características</label>
                <label className='text-regular'>{characteristics}</label>
              </div>
            </div>
            <div className='actions'>
              {mouseChat &&
                <div className='enable'>Consultá</div>
              }
              {mouseFavorite &&
                <div className='enable'>Quitar de Favoritos</div>
              }
              {mousePlus &&
                <div className='enable'>Ver más</div>
              }
              <i className="bi-chat-square-text top-text-regular" 
                onMouseEnter={() => { setMouseChat(true) }} 
                onMouseLeave={() => { setMouseChat(false) }}> 
              </i>
              <i className="bi-heartbreak top-text-regular"
                onMouseEnter={() => { setMouseFavorite(true) }} 
                onMouseLeave={() => { setMouseFavorite(false) }}
                onClick={favoriteClick}> 
              </i>
              <i className="bi-plus-circle top-text-regular" 
                onMouseEnter={() => { setMousePlus(true) }} 
                onMouseLeave={() => { setMousePlus(false) }}
                onClick={() => window.location.replace(`/vehicle?${name}`)}> 
              </i>
            </div>
        </div>
    </div>
  )
}
