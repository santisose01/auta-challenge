import React, { useContext, useState } from 'react';
import './catalog-vehicle.scss';
import { changeFavoriteVehicle } from '../../../firebase/vehicles/vehicles.ts';
import { AppContext } from '../../../contexts/AppProvider.tsx';
import { redirectNewChat } from '../../../firebase/queries/queires.ts';

export default function CatalogVehicle({ name, imageUrl }) {

  const [menu, setMenu] = useState(false);
  const { actualUser }:any = useContext(AppContext);
  const [isFavorite, setIsFavorite] = useState(actualUser?.favorite_vehicles?.includes(name));

  const favoriteClick = () => {
    changeFavoriteVehicle(actualUser.email, name, isFavorite);
    setTimeout(() => {
      setIsFavorite(!isFavorite);
    }, 2000);
  }

  const queryClick = () => {
    redirectNewChat(name);
  }

  return (
    <div className='catalog-vehicle-container' key={name}>
      <div className='body'>
        <div className='image'>
          <img src={imageUrl} alt={name}/>
        </div>
        <div className='description'>
            <label className='text-regular'>{name}</label>
            <div className='actions' onMouseEnter={() => setMenu(true)} onMouseLeave={() => setMenu(false)}>
              {
                menu && actualUser?.name &&
                <>
                  <div className='options'>
                    <div onClick={favoriteClick}>
                      {
                        isFavorite?
                        <>
                          <label className='minor-text-regular'>Quitar de Favoritos</label>
                          <i className="bi-heartbreak"></i>
                        </>
                        :
                        <>
                          <label className='minor-text-regular'>Agregar a Favoritos</label>
                          <i className="bi-heart"></i>
                        </>
                      }
                    </div>
                    {
                      actualUser.role !== 'as' &&
                      <div onClick={queryClick}>
                        <label className='minor-text-regular'>Consultar</label>
                        <i className="bi-chat-square-text"></i>
                      </div>
                    }
                    <div onClick={() => window.location.replace(`/vehicle?${name}`)}>
                      <label className='minor-text-regular'>Ver más</label>
                      <i className="bi-plus-circle"></i>
                    </div>
                  </div>
                </>
              }
              {
                actualUser?.name?
                <i className="bi-three-dots-vertical"></i>
                :
                <div className='no-logged' onClick={() => window.location.replace(`/vehicle?${name}`)}>
                  <label className='minor-text-regular'>Ver más</label>
                  <i className="bi-plus-circle"></i>
                </div>
              }
            </div>
        </div>
      </div>
    </div>
  );
}
