import React, { useState } from 'react'
import './update-vehicle-card.scss';
import { deleteVehicle, disableVehicle, enableVehicle } from '../../../firebase/vehicles/vehicles.ts';

export const UpdateVehicleCard = ({ vehicle }) => {

  const { name, initial_price, characteristics, image_url, status } = vehicle;

  //Actions States
  const [mouseEnable, setMouseEnable] = useState(false);
  const [mouseDelete, setMouseDelete] = useState(false);

  return (
    <div className='update-vehicle-card-container'>
        <div className='left-panel'>
            <div className='image'>
              <img src={image_url} alt={name} className={status}/>
            </div>
            <div className='name'>
                <label className='text-regular'>{name}</label>
            </div>
        </div>
        <div className='right-panel'>
            <div className='data'>
              <label className='text-regular initial-cost'>Precio base</label>
              <div className='price'>
                <i className="bi-currency-dollar subtitle-regular"></i>
                <label className='subtitle-regular'>{initial_price}</label>
              </div>
              <div className='characteristics'>
                <div className='title'>
                  <label className='top-text-regular'>Características</label>
                </div>
                  <label className='text-regular'>{characteristics}</label>
              </div>
            </div>
            <div className='actions'>
              {mouseEnable && (
                status === 'disabled'?
                <div className='enable'>Habilitar</div>
                :
                <div className='enable'>Deshabilitar</div>
              )}
              {mouseDelete &&
                <div className='enable'>Eliminar</div>
              }
              {status === 'disabled' &&
                <div className='disabled'>Deshabilitado</div>
              }
              {
                status === 'disabled'?
                <i className="bi-check-circle top-text-regular" 
                  onMouseEnter={() => { setMouseEnable(true) }} 
                  onMouseLeave={() => { setMouseEnable(false) }}
                  onClick={() => { enableVehicle(name) }}> 
                </i>
                :
                <i className="bi-slash-circle top-text-regular" 
                  onMouseEnter={() => { setMouseEnable(true) }} 
                  onMouseLeave={() => { setMouseEnable(false) }}
                  onClick={() => { disableVehicle(name) }}> 
                </i>
              }
              <i className="bi-trash top-text-regular" 
                onMouseEnter={() => { setMouseDelete(true) }} 
                onMouseLeave={() => { setMouseDelete(false) }}
                onClick={() => { deleteVehicle(name) }}>
              </i>
            </div>
        </div>
    </div>
  )
}
