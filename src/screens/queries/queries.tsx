import React, { useContext, useEffect, useState } from 'react'
import './queries.scss';
import Header from '../../components/cards/header/header.tsx';
import { AppContext } from '../../contexts/AppProvider.tsx';
import { assignQuery, getChat } from '../../firebase/queries/queires.ts';

export const Queries = () => {

  const { actualUser, queries }:any = useContext(AppContext);
  const [chats, setChats]:any = useState();
  const isPending = window.location.pathname !== '/queries';

  const status = {
    pending: 'Pendiente',
    closed: 'Cerrada',
    active: 'Activa'
  };
  
  useEffect(() => {
    if(isPending){
      setChats(
        queries.filter((query) => query.status === 'pending')
      )
    }else{
      let newChats:any = [];
      actualUser?.chats?.map((key) => {
        newChats.push(queries.find((query) => query.id === key));
      })
    setChats(newChats);
    }
  }, [actualUser])
  console.log(chats)
  
  return (
    <div className='queries-container'>
        <Header/>
        <div className='body'>
          <div className='title-box'>
              <label className='subtitle-bold'>{isPending? 'Consultas pendientes' : 'Tus Consultas'}</label>
          </div>
          {
            chats?.map((e) => {
              const lastChat = e?.chat[e?.chat?.length-1];
              const profile =
                lastChat?.role != actualUser?.role?
                  actualUser === 'cl'? 'Asesor:':
                  'Cliente:':
                'Tu:'
              return(
                <div className='chat-box' onClick={() => {isPending? assignQuery(e?.id, actualUser?.email): window.location.replace(`/chat?${e?.id}`)}}>
                  <div className='left-side'>
                    <label className='text-regular'>{e?.vehicle_name}. Último mensaje:</label>
                    <label className='text-regular'>{profile} {lastChat?.message}</label>
                  </div>
                  <div className='right-side'>
                    <label className='text-regular'>Estado</label>
                    <label className='text-regular'>{status[e?.status]}</label>
                  </div>
                </div>
              )
            })
          }
          {
            chats?.length === 0 &&
            <>
              <label className='top-text-regular'>No tienes consultas, puedes empezar en</label>
              <label className='top-text-bold catalog' onClick={() => window.location.replace('/catalog')}>el Catálogo {'>'}</label>
            </>
          }
        </div>
    </div>
  )
}
