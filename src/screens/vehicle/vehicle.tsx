import React, { useContext, useState } from 'react';
import './vehicle.scss';
import Header from '../../components/cards/header/header.tsx';
import { AppContext } from '../../contexts/AppProvider.tsx';
import { UpdateVehicleCard } from '../../components/cards/update-vehicle-card/update-vehicle-card.tsx';
import { useForm } from 'react-hook-form';
import db, { uploadFile } from '../../firebase/config.ts';
import { changeFavoriteVehicle, createVehicle } from '../../firebase/vehicles/vehicles.ts';
import { doc, getDoc } from 'firebase/firestore';
import { createQuery, redirectNewChat } from '../../firebase/queries/queires.ts';

export default function Vehicle() {
  
  const search = decodeURI(window.location.search.slice(1));
  const { actualUser }:any = useContext(AppContext);
  const [vehicle, setVehicle]:any = useState();
  const isFavorite = actualUser?.favorite_vehicles?.includes(vehicle?.name);

  getDoc(doc(db, 'vehicle', search)).then((resp:any) => {
    setVehicle(resp.data());
  });

  const favoriteClick = async () => {
    await changeFavoriteVehicle(actualUser?.email, vehicle?.name, isFavorite);
    setTimeout(() => {
      window.location.reload();
    }, 2000);
  }

  return (
    <div className='vehicle-container'>
      <Header/>
      <div className='internal'>
        <div className='title-box'>
          <label className='subtitle-bold'>{search}</label>
          <div>
            <i className="bi-chat-square-text top-text-regular" onClick={() => {redirectNewChat(vehicle.name)}} ></i>
            {
              isFavorite?
              <i className="bi-heartbreak top-text-regular" onClick={favoriteClick}></i>
              :
              <i className="bi-heart top-text-regular" onClick={favoriteClick}></i>
            }
          </div>
        </div>
        <div className='body'>
          <div className='image'>
            <img src={vehicle?.image_url}></img>
          </div>
            <div className='data'>
              <label className='text-regular initial-cost'>Consultá a partir de</label>
              <div className='price'>
                <i className="bi-currency-dollar subtitle-regular"></i>
                <label className='subtitle-regular'>{vehicle?.initial_price}</label>
              </div>
              <div className='characteristics'>
                <label className='top-text-bold'>Características</label>
                <label className='text-regular'>{vehicle?.characteristics}</label>
              </div>
            </div>
        </div>
      </div>
    </div>
  );
}
