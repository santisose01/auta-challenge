import React from 'react';
import './login.scss';
import Header from '../../components/cards/header/header.tsx';
import { useForm } from 'react-hook-form';
import { LoginMiddleware } from '../../firebase/auth/auth.ts';

export default function Login() {

  const {
    register,
    handleSubmit,
    formState: { errors },
    watch
  } = useForm();

  const onSubmit = handleSubmit((data) => {
    LoginMiddleware(data).then((err) => {
      if(err === 'no_exists') { alert('Este Email no está registrado') };
      if(err === 'pass_error') { alert('Contraseña incorrecta') };
    });
  });

  //Validators
  const validators = {
    required: {
      value: true,
      message: 'El campo es requerido.'
    },
    email:{
      value: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
      message: 'Correo no válido'
    },
    passwordMinlength: {
      value: 6,
      message: 'El campo debe tener mínimo 6 caracteres'
    },
    passwordMaxlength: {
      value: 30,
      message: 'El campo debe tener máximo 30 caracteres'
    }
  };

  return (
    <div className='login-container'>
      <Header />
      <div className='body'>
        <div className='form'>
          <label className='top-text-bold'>Iniciar Sesión</label>
          <form onSubmit={onSubmit}>
            <div className='input-form'>
              <label className='text-regular'>Email</label>
              <input
                type="text"
                className='form-control'
                {...register("email", {
                  required: validators.required,
                  pattern: validators.email
                })}
              />
              {errors.email && (<span>{errors.email.message?.toString()}</span>)}
            </div>
            <div className='input-form'>
              <label className='text-regular'>Contraseña</label>
              <input
                type="password"
                className='form-control'
                {...register("password", {
                  required: validators.required,
                  maxLength: validators.passwordMaxlength,
                  minLength: validators.passwordMinlength
                })}
              />
              {errors.password && (<span>{errors.password.message?.toString()}</span>)}
            </div>
            <button className='btn btn-light text-regular'>Iniciar Sesión</button>
          </form>
          <label className='text-regular'>¿No tienes una cuenta?</label>
          <button 
            className='btn btn-light text-regular'
            onClick={() => {window.location.assign(`${window.location.origin}/register`)}}
          >
            Registrarse
          </button>
        </div>
      </div>
    </div>
  );
}
