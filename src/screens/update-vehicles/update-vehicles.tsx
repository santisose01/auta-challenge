import React, { useContext, useState } from 'react';
import './update-vehicles.scss';
import Header from '../../components/cards/header/header.tsx';
import { AppContext } from '../../contexts/AppProvider.tsx';
import { UpdateVehicleCard } from '../../components/cards/update-vehicle-card/update-vehicle-card.tsx';
import { useForm } from 'react-hook-form';
import { uploadFile } from '../../firebase/config.ts';
import { createVehicle } from '../../firebase/vehicles/vehicles.ts';

export default function UpdateVehicles() {
  
  const [addVehicle, setAddVehicle] = useState(false);
  const [file, setFile]:any = useState();
  const [search, setSearch]:any = useState("");
  const { vehicles }:any = useContext(AppContext);

  let results:any = [];
  if(!search){
    results = vehicles;
  }else{
    results = vehicles.filter((vehicle) => 
      vehicle.name.toLowerCase().includes(search.toLowerCase())
    )
  }
  const searcher = (e) => {
    setSearch(e.target.value)
  }

  //React hook forms
  const {
    register,
    handleSubmit,
    formState: { errors },
    setValue,
    watch
  } = useForm();

  //Validators
  const validators = {
    required: {
      value: true,
      message: 'El campo es requerido.'
    },
    minLength: {
      value: 2,
      message: 'El campo debe tener mínimo 2 caracteres'
    },
    maxLength: {
      value: 30,
      message: 'El campo debe tener máximo 30 caracteres'
    },
    areaMaxLength:{
      value: 500,
      message: 'El campo debe tener máximo 500 caracteres'
    }
  };

  const onSubmit = handleSubmit( async (data) => {
    const imageUrl = await uploadFile(file);
    data.image_url = imageUrl;
    createVehicle(data).then((err) => {
      if(err === 'name_registered') { alert('Este Nombre de vehículo ya ha sido utilizado') }
    })
  });

  return (
    <div className='update-vehicles-container'>
      <Header/>
      <div className='internal'>
        <div className='title-box'>
          <label className='subtitle-bold'>Actualizar Vehículos</label>
          <input type='text' value={search} onChange={searcher} placeholder='Búsqueda' className='form-control text-regular' />
          <div className='add-vehicle' onClick={() => { setAddVehicle(!addVehicle) }}>
            <label className='text-regular'>Añadir</label>
            <i className="bi-plus-circle text-regular"></i>
          </div>
        </div>
        {
          addVehicle &&
          <div className='add-box'>
            <form  className='add-form' onSubmit={onSubmit}>
              <div className='left-side'>

                <div className='input-container'>
                  <label className='text-regular'>Nombre</label>
                  <input
                    type="text"
                    className='text-regular'
                    {...register("name", {
                      required: validators.required,
                      minLength: validators.minLength,
                      maxLength: validators.maxLength
                    })}
                  />
                  {errors.name && (<span>{errors.name.message?.toString()}</span>)}
                </div>

                <div className='input-container'>
                  <label className='text-regular'>Precio base</label>
                  <input
                    type="number"
                    className='text-regular'
                    {...register("initial_price", {
                      required: validators.required,
                      minLength: validators.minLength,
                      maxLength: validators.maxLength
                    })}
                  />
                  {errors.initial_price && (<span>{errors.initial_price.message?.toString()}</span>)}
                </div>

                <div className='input-container'>
                  <label className='text-regular'>Imágen</label>
                  <input
                    type="file"
                    className='minor-text-regular'
                    {...register("file", {
                      required: validators.required
                    })}
                    onChange={(e) => {
                      setFile(e.target.files && e.target.files[0]);
                    }}
                  />
                  {errors.file && (<span>{errors.file.message?.toString()}</span>)}       
                </div>

              </div>
              <div className='right-side'>
                <label className='text-regular'>Características</label>
                <textarea
                  className='text-regular'
                  {...register("characteristics", {
                    required: validators.required,
                    minLength: validators.minLength,
                    maxLength: validators.areaMaxLength
                  })}
                />
                {errors.characteristics && (<span>{errors.characteristics.message?.toString()}</span>)} 
              </div>
              <div className='icon-container'>
                <i onClick={onSubmit} className="bi-check-circle-fill top-text-regular"></i>
              </div>
            </form>
          </div>
        }
        <div className='vehicles'>
          {
            results[0]?
            results.map((vehicle) => (
              <div className='vehicle'>
                <UpdateVehicleCard 
                  key={vehicle.id} 
                  vehicle={vehicle}
                  />
              </div>
            ))
            :
            <label className='text-regular'>No hay vehículos</label>
          }
        </div>
      </div>
    </div>
  );
}
