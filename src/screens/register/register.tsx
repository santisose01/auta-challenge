import React from 'react';
import './register.scss';
import Header from '../../components/cards/header/header.tsx';
import { useForm } from 'react-hook-form';
import { RegisterMiddleware } from '../../firebase/auth/auth.ts';

export default function Register() {

  const {
    register,
    handleSubmit,
    formState: { errors },
    watch
  } = useForm();

  const onSubmit = handleSubmit((data) => {
    RegisterMiddleware(data).then((err) => {
      if(err === 'email_registered') { alert('Este Email ya tiene una cuenta') }
    });
  });

  //Validators
  const validators = {
    required: {
      value: true,
      message: 'El campo es requerido.'
    },
    nameMinlength: {
      value: 2,
      message: 'El campo debe tener mínimo 2 caracteres'
    },
    nameMaxlength: {
      value: 20,
      message: 'El campo debe tener máximo 20 caracteres'
    },
    email:{
      value: /^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/,
      message: 'Correo no válido'
    },
    passwordMinlength: {
      value: 6,
      message: 'El campo debe tener mínimo 6 caracteres'
    },
    passwordMaxlength: {
      value: 30,
      message: 'El campo debe tener máximo 30 caracteres'
    }
  };
  console.log(watch())

  return (
    <div className='register-container'>
      <Header />
      <div className='body'>
        <div className='form'>
          <label className='top-text-bold'>Registrarse</label>
          <form onSubmit={onSubmit}>
            <div className='input-form'>
              <label className='text-input'>Nombre</label>
              <input
                type="text"
                className='form-control'
                {...register("name", {
                  required: validators.required,
                  maxLength: validators.nameMaxlength,
                  minLength: validators.nameMinlength
                })}
              />
              {errors.name && (<span>{errors.name.message?.toString()}</span>)}
            </div>
            <div className='input-form'>
              <label className='text-input'>Apellido</label>
              <input
                type="text"
                className='form-control'
                {...register("lastname", {
                  required: validators.required,
                  maxLength: validators.nameMaxlength,
                  minLength: validators.nameMinlength
                })}
              />
              {errors.lastname && (<span>{errors.lastname.message?.toString()}</span>)}
            </div>
            <div className='input-form'>
              <label className='text-input'>Email</label>
              <input
                type="text"
                className='form-control'
                {...register("email", {
                  required: validators.required,
                  pattern: validators.email
                })}
              />
              {errors.email && (<span>{errors.email.message?.toString()}</span>)}
            </div>
            <div className='input-form'>
              <label>Contraseña</label>
              <input
                type="password"
                className='form-control'
                {...register("password", {
                  required: validators.required,
                  maxLength: validators.passwordMaxlength,
                  minLength: validators.passwordMinlength
                })}
              />
              {errors.password && (<span>{errors.password.message?.toString()}</span>)}
            </div>
            <div className='input-form'>
              <label className='text-input'>Confirmar Contraseña</label>
              <input
                type="password"
                className='form-control'
                {...register("confirm_password", {
                  validate: value => value === watch('password') || 'Las contraseñas no coinciden'
                })}
              />
              {errors.confirm_password && (<span>{errors.confirm_password.message?.toString()}</span>)}
            </div>
            <label className='text-input'>Rol</label>
            <select className='form-select form-select-sm' {...register("role")}>
              <option value='cl'>Cliente</option>
              <option value='as'>Asesor</option>
            </select>
            <button className='btn btn-light text-regular'>Registrarse</button>
            </form>
            <label className='text-input'>¿Ya tienes una cuenta?</label>
            <button 
            className='btn btn-light text-regular'
              onClick={() => {window.location.assign(`${window.location.origin}/login`)}}
            >
              Iniciar Sesión
            </button>
          </div>
      </div>
    </div>
  );
}
