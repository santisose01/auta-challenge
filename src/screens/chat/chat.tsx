import React, { useContext, useEffect, useState } from 'react'
import './chat.scss';
import Header from '../../components/cards/header/header.tsx';
import { useForm } from 'react-hook-form';
import { AppContext } from '../../contexts/AppProvider.tsx';
import { createQuery, getChat, sendMessage } from '../../firebase/queries/queires.ts';

export const Chat = () => {

  const { actualUser }:any = useContext(AppContext);
  const vehicleName = decodeURI(window.location.search).slice(9);
  const windowSearch = decodeURI(window.location.search).slice(1,8);
  const key = window.location.search.slice(1);

  const [isNew, setIsNew] = useState(false);
  const [chat, setChat]:any = useState();

  const status = {
    pending: 'pendiente (todavia sin contestar)',
    closed: 'cerrado (el asesor cerró el chat)',
    active: 'activo'
  };

  const {
    register,
    handleSubmit,
    reset
  } = useForm();

  const onSubmit = handleSubmit((data) => {
    if(isNew){
      createQuery(actualUser.email, vehicleName, data.message)
    }else{
      let newChat = chat;
      newChat.chat.push({
        role: actualUser.role,
        message: data.message
      })
      setChat(newChat);
      sendMessage(key, data.message, actualUser.role)
      reset();
    }
  });

  useEffect(() => {
    if(windowSearch === 'vehicle'){
      setIsNew(true);
    }else{
      getChat(key).then((data) => {
        setChat(data);
      });
    }
  }, [])
  console.log(chat)

  return (
    <div className='chat-container'>
      <Header/>
      <div className='body'>
        <div className='title-box'>
          <label className='subtitle-bold'>{chat?.vehicle_name?? vehicleName}</label>
          <label className='text-regular'>Estado: {status[chat?.status]}</label>
          {
            chat?.status === 'active' &&
            <label className='text-regular'>Chateando con: {actualUser.role === 'cl'? chat?.cl_email: chat?.as_email}</label>
          }
        </div>
        <div className='chat-box'>
            <div className='background'>
              {
                chat?.chat?.map((msg) => {
                  if(msg.role === actualUser.role){
                    return(
                      <div className='own-message'>
                        <label className='text-regular'>{msg.message}</label>
                      </div>
                    )
                  }else{
                    return(
                      <div className='other-message'>
                        <label className='text-regular'>{msg.message}</label>
                      </div>
                    )
                  }
                })
              }
            </div>
            <form onSubmit={onSubmit} className='message'>
              <input
                type="text"
                className='text-regular'
                placeholder='Escribe un mensaje...'
                {...register("message")}
              />
              <i onClick={onSubmit} className="bi-send text-regular"/>
            </form>
        </div>
      </div>
    </div>
  )
}
