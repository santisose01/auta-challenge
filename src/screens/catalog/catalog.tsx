import React, { useContext, useState } from 'react';
import './catalog.scss';
import Header from '../../components/cards/header/header.tsx';
import CatalogVehicle from '../../components/cards/catalog-vehicle/catalog-vehicle.tsx';
import { AppContext } from '../../contexts/AppProvider.tsx';

export default function Catalog() {
  
  const { vehicles }:any = useContext(AppContext);
  const [search, setSearch]:any = useState("");

  let results:any = [];
  if(!search){
    results = vehicles;
  }else{
    results = vehicles.filter((vehicle) => 
      vehicle.name.toLowerCase().includes(search.toLowerCase())
    )
  }
  const searcher = (e) => {
    setSearch(e.target.value)
  }

  return (
    <div className='catalog-container'>
      <Header/>
      <div className='body'>
        <div className='title-box'>
          <label className='subtitle-bold'>Catálogo</label>
          <input type='text' value={search} onChange={searcher} placeholder='Búsqueda' className='form-control text-regular' />
        </div>
        <div className='vehicles'>
          {
            results.map((vehicle) => (
              vehicle.status !== 'disabled' &&
              <CatalogVehicle 
              key={vehicle.name} 
              name={vehicle.name} 
              imageUrl={vehicle.image_url}
              />
            ))
          }
          {
            results.length === 0 &&
            <label className='top-text-regular'>Lo sentimos! No hay vehículos disponibles</label>
          }
        </div>
      </div>
    </div>
  );
}
