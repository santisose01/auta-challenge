import React, { useContext, useEffect, useState } from 'react';
import './favorites.scss';
import Header from '../../components/cards/header/header.tsx';
import { AppContext } from '../../contexts/AppProvider.tsx';
import { FavoriteCard } from '../../components/cards/favorite-card/favorite-card.tsx';
import { deleteAllFavoriteVehicles } from '../../firebase/vehicles/vehicles.ts';

export default function Favorites() {
  
  const { actualUser, vehicles }:any = useContext(AppContext);

  let favoriteVehicles:any = [];
  actualUser.favorite_vehicles?.map((name) => {
    favoriteVehicles.push(vehicles.find((e) => e.name === name))
  });

  const trashClick = () => {
    deleteAllFavoriteVehicles(actualUser.email);
    setTimeout(() => {
      window.location.reload();
    }, 2000);
  };

  return (
    <div className='favorites-container'>
      <Header/>
      <div className='body'>
        <div className='title-box'>
          <label className='subtitle-bold'>Tus Vehículos Favoritos</label>
            <div className='delete-favorites' onClick={trashClick}>
              <label className='text-regular'>Eliminar Todos</label>
              <i className="bi-trash text-regular"></i>
            </div>
        </div>
        <div className='vehicles'>
          {
            favoriteVehicles.length > 0?
            favoriteVehicles.map((vehicle:any) => (
              <div className='vehicle'>
                <FavoriteCard
                  key={vehicle.name}
                  vehicle={vehicle}
                  userEmail={actualUser?.email}
                />
              </div>
            ))
            :
            <>
              <label className='top-text-regular'>No tienes vehículos favoritos, puedes agregar en</label>
              <label className='top-text-bold catalog' onClick={() => window.location.replace('/catalog')}>el Catálogo {'>'}</label>
            </>
          }
        </div>
      </div>
    </div>
  );
}
