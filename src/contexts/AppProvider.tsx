import React, { createContext, useEffect, useState } from 'react';
import { collection, getDocs, doc, getDoc } from "firebase/firestore";
import db from '../firebase/config.ts';
import { validateToken } from '../firebase/jwtoken/jwtoken.ts';

export const AppContext = createContext({});

export const AppProvider = ({ children }) => {

  const [actualUser, setActualUser] = useState({});
  const [vehicles, setVehicles] = useState([]);
  const [queries, setQueries] = useState([]);

  useEffect(() => {

    //Validate Session Token
    validateToken().then((user:any) => {
      if(user){
        getDoc(doc(db, 'user', user.email)).then((resp:any) => {
          setActualUser(resp.data());
        })
      }
    });

    //Vehicles requested from the database
    const vehiclesRef = collection(db, 'vehicle');
    getDocs(vehiclesRef)
      .then((resp:any) => {
        let vehicles:any = []
        resp.docs.forEach((doc) => {
          vehicles.push({ ...doc.data(), id: doc.id })
        })
        setVehicles(vehicles)
      });

    //Queries requested from the database
    const queriesRef = collection(db, 'query');
    getDocs(queriesRef)
      .then((resp:any) => {
        let queries:any = []
        resp.docs.forEach((doc) => {
          queries.push({ ...doc.data(), id: doc.id })
        })
        setQueries(queries)
      });

  }, []);

  return (
    <AppContext.Provider value={{actualUser, vehicles, queries}}>
      {children}
    </AppContext.Provider>
  )
}
